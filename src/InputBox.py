import pygame


COLOR = pygame.Color('blueviolet')


flag = False
player_name = ""


class InputBox:

    def __init__(self, x, y, w, h, font, text=''):
        self.rect = pygame.Rect(x, y, w, h)
        self.myfont = font
        self.color = COLOR
        self.text = text
        self.txt_surface = self.myfont.render(text, True, self.color)

    def handle_event(self, event):
        global flag
        global player_name

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                player_name = self.text
                self.text = ''
                flag = True
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[: -1]
            else:
                self.text += event.unicode
            self.txt_surface = self.myfont.render(self.text, True, self.color)

    def update(self):
        width = max(300, self.txt_surface.get_width() + 10)
        self.rect.w = width

    def draw(self, screen):
        screen.blit(self.txt_surface, (self.rect.x, self.rect.y - 10))
        pygame.draw.rect(screen, self.color, self.rect, 2)
