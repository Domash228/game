import pygame
from random import randrange


image = pygame.image.load('images/shipEnemy2.png')


class Enemy(pygame.sprite.Sprite):
    speed = 10

    def __init__(self, x, y):

        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect(center=(x, 0))
        self.rect.x = x
        self.rect.y = y
        self.type_move = randrange(2)

    def update(self):
        self.rect.y += self.speed
        if self.rect.y > 900:
            self.kill()

        if self.type_move == 0:
            if self.rect.x + 75 < 930:
                self.rect.x += self.speed
            else:
                self.type_move = 1
        elif self.type_move == 1:
            if self.rect.x - self.speed > 0:
                self.rect.x -= self.speed
            else:
                self.type_move = 0
        else:
            type_move = randrange(2)
            if type_move == 0 and self.rect.x + 75 < 930:
                self.rect.x += self.speed
            elif type_move == 1 and self.rect.x - self.speed > 0:
                self.rect.x -= self.speed
