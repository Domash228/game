import pygame
import hero
import enemy
import InputBox
from pymongo import MongoClient
from random import randrange


pygame.init()
pygame.font.init()

myfont = pygame.font.SysFont('Comic Sans MS', 30)
button_font = pygame.font.SysFont('Comic Sans MS', 50)


screen = pygame.display.set_mode((930, 886), pygame.FULLSCREEN)
pygame.display.set_caption("Game")

client = MongoClient()
db = client.mydatabase
records = db.records

run = True

type_screen = 0

isFirst = True

x = 0
cd = 0
score = 0
background_image = pygame.image.load('images/background.jpg').convert_alpha()


shift = hero.Hero(465 - 75, 886 - 120)

enemyGroup = pygame.sprite.Group()

clock = pygame.time.Clock()


text_score = myfont.render(f'Score : {score}', True, (255, 255, 255))
text_hp = myfont.render(f'HP : {shift.hp}', True, (255, 255, 255))

text_new_Game = button_font.render('New Game', True, (255, 255, 255), (0, 0, 128))
text_new_game_rect = text_new_Game.get_rect()
text_new_game_rect.center = (100, 886 // 2 - 90)

text_exit = button_font.render('Exit', True, (255, 255, 255), (0, 0, 128))
text_exit_rect = text_new_Game.get_rect()
text_exit_rect.center = (100, 886 // 2 + 30)

text_continue = button_font.render('Continue', True, (255, 255, 255), (0, 0, 128))
text_continue_rect = text_continue.get_rect()
text_continue_rect.center = (930 // 2, 886 // 2)

text_records = button_font.render('Records', True, (255, 255, 255), (0, 0, 128))
text_records_rect = text_records.get_rect()
text_records_rect.center = (100, 886 // 2 - 30)

text_records_rect.x = 100
text_exit_rect.x = 100
text_new_game_rect.x = 100


all_records = [myfont.render(f'{i + 1})', True, pygame.Color('blueviolet')) for i in range(20)]


input_box = InputBox.InputBox(465 - 150, 443 - 20, 140, 50, button_font)


def name_input():
    global type_screen

    input_box.update()

    screen.fill((15, 15, 15))

    input_box.draw(screen)

    pygame.display.flip()

    if InputBox.flag:
        type_screen = 1
        InputBox.flag = False


def records_screen():
    global isFirst
    global type_screen

    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if isFirst:
        new_records = [current for current in records.find().sort("Score")]
        # print(record)

        for i in range(min(20, records.find().count())):
            all_records[i] = myfont.render(f'{i + 1}) {new_records[-1 - i]["Name"]} : {new_records[-1 - i]["Score"]}', True,
                                           pygame.Color('blueviolet'))

        isFirst = False

    if text_continue_rect.x <= mouse[0] <= text_continue_rect.x + text_continue_rect.width and text_continue_rect.y <= \
            mouse[1] <= text_continue_rect.y + text_continue_rect.height and click[0] == 1:
        type_screen = 0

    screen.fill((15, 15, 15))
    for i in range(20):
        screen.blit(all_records[i], (5, 50 + i * 40))
    screen.blit(text_continue, text_continue_rect)
    pygame.display.update()


def game_over():
    global score
    global type_screen
    global enemyGroup
    global text_hp
    global text_score

    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    text_end_score = button_font.render(f'Score : {score}', True, (255, 255, 255), (0, 0, 128))
    text_end_score_rect = text_end_score.get_rect()
    text_end_score_rect.center = (930 // 2, 886 // 2 + 60)

    if text_continue_rect.x <= mouse[0] <= text_continue_rect.x + text_continue_rect.width and text_continue_rect.y <= \
            mouse[1] <= text_continue_rect.y + text_continue_rect.height and click[0] == 1:
        type_screen = 0
        score = 0
        text_score = myfont.render(f'Score : {score}', True, (255, 255, 255))
        shift.new_game()
        text_hp = myfont.render(f'HP : {shift.hp}', True, (255, 255, 255))
        enemyGroup = pygame.sprite.Group()

    screen.fill((15, 15, 15))
    screen.blit(text_continue, text_continue_rect)
    screen.blit(text_end_score, text_end_score_rect)
    pygame.display.update()


def menu():
    global type_screen
    global run

    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if text_new_game_rect.x <= mouse[0] <= text_new_game_rect.x + text_new_game_rect.width and text_new_game_rect.y <= \
            mouse[1] <= text_new_game_rect.y + text_new_game_rect.height and click[0] == 1:
        type_screen = 3

    if text_records_rect.x <= mouse[0] <= text_records_rect.x + text_records_rect.width and text_records_rect.y <= \
            mouse[1] <= text_records_rect.y + text_records_rect.height and click[0] == 1:
        type_screen = 4

    if text_exit_rect.x <= mouse[0] <= text_exit_rect.x + text_exit_rect.width and text_exit_rect.y <= \
            mouse[1] <= text_exit_rect.y + text_exit_rect.height and click[0] == 1:
        run = False

    screen.fill((15, 15, 15))
    screen.blit(text_new_Game, text_new_game_rect)
    screen.blit(text_exit, text_exit_rect)
    screen.blit(text_records, text_records_rect)
    pygame.display.update()


def play():

    global cd
    global x
    global score
    global text_score
    global text_hp
    global run
    global type_screen
    global isFirst

    if cd == 0:
        cd = 6
        if randrange(2) % 2 == 0:
            enemyGroup.add(enemy.Enemy(randrange(930 - 75), -49))
    cd -= 1

    if -886 + x > 0:
        x = 0

    if len(pygame.sprite.groupcollide(enemyGroup, shift.bullets, True, True)):
        score += 5
        text_score = myfont.render(f'Score : {score}', True, (255, 255, 255))

    if len(pygame.sprite.spritecollide(shift, enemyGroup, True)):
        shift.hp -= 1
        if shift.hp == 0:
            type_screen = 2
            isFirst = True
            records.insert_one({"Name": InputBox.player_name, "Score": score})
            return
        text_hp = myfont.render(f'HP : {shift.hp}', True, (255, 255, 255))

    screen.blit(background_image, (0, x))
    screen.blit(background_image, (0, -886 + x))
    x += 10
    screen.blit(shift.image, shift.rect)
    enemyGroup.draw(screen)
    shift.bullets.draw(screen)
    screen.blit(text_score, (0, 0))
    screen.blit(text_hp, (840, 0))

    shift.update()
    enemyGroup.update()
    shift.bullets.update()
    pygame.display.update()

#hello

while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if type_screen == 3:
            input_box.handle_event(event)

    if type_screen == 0:
        menu()
    elif type_screen == 1:
        play()
    elif type_screen == 2:
        game_over()
    elif type_screen == 3:
        name_input()
    elif type_screen == 4:
        records_screen()

    clock.tick(60)

pygame.quit()
