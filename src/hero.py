import pygame
import bullet


image = pygame.image.load('images/mySpaceship.png')


class Hero(pygame.sprite.Sprite):

    speed = 10
    bullets = pygame.sprite.Group()
    cd = 0
    hp = 3

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect(center=(x, 0))
        self.rect.x = x
        self.rect.y = y
        self.start_x = x
        self.start_y = y

    def new_game(self):
        self.hp = 3
        self.cd = 0
        self.rect.x = self.start_x
        self.rect.y = self.start_y
        self.bullets = pygame.sprite.Group()

    def update(self):

        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:
            self.rect.x -= self.speed
            if self.rect.x < 0:
                self.rect.x += self.speed
        if keys[pygame.K_RIGHT]:
            self.rect.x += self.speed
            if self.rect.x + 150 > 930:
                self.rect.x -= self.speed
        if keys[pygame.K_UP]:
            self.rect.y -= self.speed
            if self.rect.y < 0:
                self.rect.y += self.speed
        if keys[pygame.K_DOWN]:
            self.rect.y += self.speed
            if self.rect.y + 120 > 886:
                self.rect.y -= self.speed
        if keys[pygame.K_SPACE] and self.cd == 0:
            self.bullets.add(bullet.Bullet(self.rect.x + 70, self.rect.y))
            self.cd = 10

        if self.cd > 0:
            self.cd -= 1
