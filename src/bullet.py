import pygame


image = pygame.image.load('images/myBullet2.png')


class Bullet(pygame.sprite.Sprite):

    speed = 16

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect(center=(x, 0))
        self.rect.x = x
        self.rect.y = y

    def update(self):
        self.rect.y -= self.speed
        if self.rect.y < -5:
            self.kill()
